<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>de.maddevs</groupId>
    <artifactId>mad-linguist</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>MadLinguist</name>

    <description>
        An editor for creating dictionaries.
    </description>
    <url>https://maddevs.de/</url>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <developers>
        <developer>
            <id>Drayke</id>
            <name>Kevin Mattutat</name>
            <email>drayke@maddevs.de</email>
            <url>http://www.drayke.de</url>
            <roles>
                <role>creator</role>
                <role>developer</role>
            </roles>
        </developer>
    </developers>

    <dependencies>
        <!-- Lombok - for annotations -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.16.4</version>
            <scope>provided</scope>
        </dependency>
        <!-- MadDevs Translator-API -->
        <dependency>
            <groupId>de.maddevs</groupId>
            <artifactId>translator-api</artifactId>
            <version>1.0</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <!-- Standard compiler for later jar -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
                <executions>
                    <execution>
                        <id>default-testCompile</id>
                        <phase>test-compile</phase>
                        <configuration>
                            <testExcludes>
                                <exclude>**/*.java</exclude>
                            </testExcludes>
                        </configuration>
                        <goals>
                            <goal>testCompile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Shade TranslatorAPI into JAR file -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>1.7.1</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <shadedArtifactAttached>true</shadedArtifactAttached>
                    <shadedClassifierName>shaded</shadedClassifierName> <!-- prefix -->
                    <transformers>
                        <transformer
                                implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                            <mainClass>de.maddevs.linguist.Bootstrap</mainClass>
                        </transformer>
                    </transformers>
                </configuration>
            </plugin>

            <!-- Creating an .exe file -->
            <plugin>
                <groupId>com.akathist.maven.plugins.launch4j</groupId>
                <artifactId>launch4j-maven-plugin</artifactId>
                <version>1.7.18</version>
                <executions>
                    <execution>
                        <id>mad-linguist</id>
                        <phase>package</phase>
                        <goals>
                            <goal>launch4j</goal>
                        </goals>
                        <configuration>
                            <headerType>gui</headerType>
                            <outfile>target/MadLinguist.exe</outfile>
                            <jar>target/${project.artifactId}-${project.version}-shaded.jar
                            </jar> <!-- don't miss shaded prefix -->
                            <dontWrapJar>false</dontWrapJar>
                            <errTitle>Error in MadLinguist.exe</errTitle>
                            <classPath>
                                <mainClass>de.maddevs.linguist.Bootstrap</mainClass>
                            </classPath>
                            <icon>${project.basedir}/src/main/resources/favicon.ico</icon>
                            <jre>
                                <minVersion>1.8.0</minVersion> <!-- because lambda is used -->
                                <initialHeapSize>512</initialHeapSize>
                                <maxHeapSize>1024</maxHeapSize>
                            </jre>
                            <!-- Executeable Details -->
                            <versionInfo>
                                <fileVersion>1.0.0.1</fileVersion>
                                <txtFileVersion>1.0.0.1</txtFileVersion>
                                <fileDescription>des</fileDescription>
                                <copyright>Copyright (c) 2017</copyright>
                                <companyName>MadDevs</companyName>
                                <productVersion>1.0.0.1</productVersion>
                                <txtProductVersion>${project.version}</txtProductVersion>
                                <productName>MadLinguist</productName>
                                <internalName>MadLinguist</internalName>
                                <originalFilename>MadLinguist.exe</originalFilename>
                            </versionInfo>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>