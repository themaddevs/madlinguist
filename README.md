# README #

This project uses the [TranslatorAPI](https://bitbucket.org/themaddevs/translatorapi) to find translations within a java project.
The Editor has been created with JavaFX and scans given source files for usage of tr("Translation") method calls.

## Install ##

Clone [TranslatorAPI](https://bitbucket.org/themaddevs/translatorapi) and run maven::install.
 
Execute maven::package on this project and run the MadLinguist.exe.

### You can also download the prebuild executeable [here](http://drayke.de/download/madlinguist/) ###