package de.maddevs.linguist.cell;

import de.maddevs.linguist.dialogs.ExceptionDialog;
import de.maddevs.translator.api.Dictionary;
import de.maddevs.translator.core.OriginalText;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 15.06.2017
 */
public final class OriginalTextCell extends ListCell<OriginalText>
{

    @FXML
    private GridPane gridPane;

    @FXML
    private Label originalText;

    @FXML
    private Label counter;

    private FXMLLoader mLLoader;

    @Override
    protected void updateItem( OriginalText text, boolean empty )
    {
        super.updateItem( text, empty );

        if ( empty || text == null )
        {

            setText( null );
            setGraphic( null );

        }
        else
        {
            if ( mLLoader == null )
            {
                mLLoader = new FXMLLoader( getClass().getClassLoader().getResource( "ui/OriginalTextCell.fxml" ) );
                mLLoader.setController( this );
                try
                {
                    mLLoader.load();
                }
                catch ( Exception e )
                {
                    new ExceptionDialog( e );
                }
            }

            String add = "";
            if ( !text.getTranslations().isEmpty() )
                if ( !text.getTranslations().get( 0 ).isReplaceable() )
                    add = " (protected)";

            originalText.setText( text.getText() );
            int sizeT = text.getTranslations().size();
            int sizeL = Dictionary.getInstance().getLanguages().size();

            counter.setText( sizeT + "/" + sizeL + add );
            if ( sizeT < sizeL )
                counter.setTextFill( Color.RED );
            else
                counter.setTextFill( Color.BLACK );

            setText( null );
            setGraphic( gridPane );
        }

    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/