package de.maddevs.linguist.cell;


import de.maddevs.linguist.container.TranslationText;
import de.maddevs.linguist.dialogs.ExceptionDialog;
import de.maddevs.translator.api.Dictionary;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;


/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 15.06.2017
 */
public final class TranslationCell extends ListCell<TranslationText>
{

    @FXML
    private GridPane gridPane;
    @FXML
    private Label languageLbl;
    @FXML
    private TextField translationInputField;
    @FXML
    private ImageView icon;
    private FXMLLoader fxmlLoader;

    private TranslationText translation;

    @Override
    protected void updateItem( TranslationText translation, boolean empty )
    {
        super.updateItem( translation, empty );

        if ( empty || translation == null )
        {

            setText( null );
            setGraphic( null );

        }
        else
        {
            if ( fxmlLoader == null )
            {
                fxmlLoader = new FXMLLoader( getClass().getClassLoader().getResource( "ui/TranslationCell.fxml" ) );
                fxmlLoader.setController( this );
                try
                {
                    fxmlLoader.load();
                }
                catch ( Exception e )
                {
                    new ExceptionDialog( e );
                }

                //initial
                this.translation = translation;
            }


            this.translationInputField.textProperty().addListener( ( observable, oldValue, newValue ) -> changeTranslation( newValue ) );

            languageLbl.setText( Dictionary.getInstance().getLanguage( translation.getLanguageKey() ).getLanguageName() );

            translationInputField.setText( "" );
            if ( !translation.getTranslatedText().isEmpty() )
                translationInputField.setText( translation.getTranslatedText() );
            else
                translationInputField.setPromptText( translation.getOriginalText() );


            setText( null );
            setGraphic( gridPane );
        }

    }

    private void changeTranslation( String newValue )
    {
        if ( this.translation != null )
        {
            this.translation.setTranslatedText( newValue );
        }
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/