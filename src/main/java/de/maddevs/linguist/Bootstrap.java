package de.maddevs.linguist;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.Getter;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * This is the first entered class when running the jar executable.
 * It's the bootstrap class to load and launch the editor.
 * </p>
 *
 * @author Drayke
 * @version 1.0
 * @since 14.07.2017
 */
public final class Bootstrap extends Application
{

    @Getter
    private static boolean instantiated = false;

    /**
     * The entry point of application.
     *
     * @param args the input arguments (commonly unused)
     */
    public static void main( String[] args )
    {
        launch( args );
    }

    @Override
    public void start( Stage primaryStage ) throws Exception
    {
        //Load Layout for root UI
        Parent root = FXMLLoader.load( getClass().getClassLoader().getResource( "Editor.fxml" ) );
        //Configure UI
        primaryStage.setTitle( "MadLinguist - Translator" );
        primaryStage.setScene( new Scene( root, 550, 475 ) );
        primaryStage.getIcons().add( new Image( Bootstrap.class.getResourceAsStream( "/ui/assets/translator.jpg" ) ) );
        primaryStage.setMaximized( true );
        //Show
        primaryStage.show();
        instantiated = true;
    }


}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/