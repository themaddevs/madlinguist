package de.maddevs.linguist.container;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * This class is a container to edit
 * translation of a (@code OriginalText).
 *
 * @author Drayke
 * @version 1.0
 * @since 19.07.2017
 */
@Data
@AllArgsConstructor
public class TranslationText
{

    private String languageKey;

    private String translatedText;

    private String originalText;

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/