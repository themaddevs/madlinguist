package de.maddevs.linguist.controller;


import de.maddevs.linguist.cell.OriginalTextCell;
import de.maddevs.linguist.cell.TranslationCell;
import de.maddevs.linguist.container.TranslationText;
import de.maddevs.linguist.dialogs.CreateLanguageDialog;
import de.maddevs.linguist.dialogs.ExceptionDialog;
import de.maddevs.linguist.util.*;
import de.maddevs.translator.api.DictionaryFile;
import de.maddevs.translator.api.Translator;
import de.maddevs.translator.core.Language;
import de.maddevs.translator.core.OriginalText;
import de.maddevs.translator.core.Translation;
import de.maddevs.translator.util.Pair;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import lombok.Getter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 14.06.2017
 */
public final class MainController
{

    @Getter
    private static MainController instance;

    @Getter
    private static ObservableList<OriginalText> originalTextObservableList;
    @Getter
    private static ObservableList<TranslationText> translationObservableList;

    @Getter
    @FXML // fx:id="originalTextList"
    private ListView<OriginalText> originalTextList; // Value injected by FXMLLoader

    @Getter
    @FXML // fx:id="translationList"
    private ListView<TranslationText> translationList; // Value injected by FXMLLoader

    @FXML
    private TextArea fullOriginalTextField;

    @FXML //id:protectedCheckBox
    private CheckBox protectedCheckBox;

    /**
     * Initialize.
     */
    @FXML
    // This method is called by the FXMLLoader when initialization is complete
    void initialize()
    {
        instance = this;

        originalTextObservableList = FXCollections.observableArrayList();
        translationObservableList = FXCollections.observableArrayList();

        originalTextList.setCellFactory( originalTextCell -> new OriginalTextCell() );
        translationList.setCellFactory( translationCell -> new TranslationCell() );

        originalTextList.setItems( originalTextObservableList );
        translationList.setItems( translationObservableList );
    }

    /**
     * Scan file btn clicked.
     *
     * @param event the event
     */
    @FXML
    void scanFileBtnClicked( ActionEvent event )
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle( "Open source file (.java)" );
        fileChooser.setInitialDirectory( new File( Settings.getInstance().getPath( Settings.ESetting.LAST_OPEN ) ) );
        File file = fileChooser.showOpenDialog( StageFactory.INSTANCE.createStage() );
        if ( file == null ) return;
        if ( file.isFile() )
        {
            Settings.getInstance().savePath( Settings.ESetting.LAST_OPEN, file.getAbsolutePath() );
            List<OriginalText> scan = SourceScanner.getInstance().scan( file );
            Translator.getDictionary().getEntries().addAll( scan );
            reloadView();
        }
    }

    /**
     * Scan directory btn clicked.
     *
     * @param event the event
     */
    @FXML
    void scanDirectoryBtnClicked( ActionEvent event )
    {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle( "Open source folder" );
        directoryChooser.setInitialDirectory( new File( Settings.getInstance().getPath( Settings.ESetting.LAST_OPEN ) ) );
        File file = directoryChooser.showDialog( StageFactory.INSTANCE.createStage() );
        if ( file == null ) return;
        if ( file.isDirectory() )
        {
            Settings.getInstance().savePath( Settings.ESetting.LAST_OPEN, file.getAbsolutePath() );
            List<OriginalText> scan = SourceScanner.getInstance().scan( file );
            Translator.getDictionary().getEntries().addAll( scan );
            reloadView();
        }
    }


    /**
     * Open translation view.
     *
     * @param event the event
     */
    @FXML
    void openTranslationView( MouseEvent event )
    {
        OriginalText selectedItem = originalTextList.getSelectionModel().getSelectedItem();
        if ( selectedItem != null )
        {

            this.translationObservableList.clear();
            this.fullOriginalTextField.setText( selectedItem.getText() );
            this.translationObservableList.addAll( generate( selectedItem ) );

            if ( !selectedItem.getTranslations().isEmpty() )
                this.protectedCheckBox.setSelected( !selectedItem.getTranslations().get( 0 ).isReplaceable() );
            else
                this.protectedCheckBox.setSelected( true );
            //this.translationList.refresh();
        }

    }

    /**
     * Translation done.
     *
     * @param event the event
     */
    @FXML
    void translationDone( ActionEvent event )
    {
        OriginalText selectedItem = originalTextList.getSelectionModel().getSelectedItem();
        if ( selectedItem != null )
        {
            transform( selectedItem, new ArrayList<>( this.translationObservableList ), protectedCheckBox.isSelected() );
            reloadView();
        }

    }

    /**
     * Show current languages.
     *
     * @param event the event
     */
    @FXML
    void showCurrentLanguages( ActionEvent event )
    {
        new ListDialog<Language>( "Available languages", "Currently available languages for translation:",
                new ArrayList<>( Translator.getDictionary().getLanguages() ) );
    }

    /**
     * Add language.
     *
     * @param event the event
     */
    @FXML
    public void addLanguage( ActionEvent event )
    {
        CreateLanguageDialog dialog = new CreateLanguageDialog();
        Optional<Language> language = dialog.showAndWait();
        if ( language.isPresent() )
        {
            Language lang = language.get();
            Translator.getDictionary().getLanguages().add( lang );
            Toast.toast( "Added: " + lang.getLanguageName() );
            this.originalTextList.refresh(); //to display the correct amount of already translated texts
        }
    }

    /**
     * Build dictionary.
     *
     * @param event the event
     */
    @FXML
    public void buildDictionary( ActionEvent event )
    {

        if ( Translator.getDictionary().getEntries().isEmpty() )
        {
            Toast.toast( "Keine Translations gefunden!" );
            return;
        }

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle( "Chose your directory for saving the dictionary" );
        directoryChooser.setInitialDirectory( new File( Settings.getInstance().getPath( Settings.ESetting.LAST_SAVE ) ) );
        File file = directoryChooser.showDialog( StageFactory.INSTANCE.createStage() );

        if ( file == null ) return;

        DictionaryFile dictionaryFile = null;
        try
        {
            File dict = new File( file, "dictionary.xml" );
            System.out.println( dict.getAbsolutePath() );
            if ( dict.exists() )
            {
                //Do you want to overwrite?
                ButtonType yes = new ButtonType( "YES", ButtonBar.ButtonData.OK_DONE );
                ButtonType no = new ButtonType( "NO", ButtonBar.ButtonData.CANCEL_CLOSE );
                Alert alert = new Alert( Alert.AlertType.WARNING,
                        "Do you want to overwrite the already existing dictionary?", yes, no );
                alert.setTitle( "File dictionary.xml already exists." );

                Optional<ButtonType> buttonType = alert.showAndWait();
                if ( buttonType.isPresent() && buttonType.get() == yes )
                {
                    boolean delete = dict.delete();
                    if ( !delete )
                    {
                        new ExceptionDialog( new Exception( "Could not delete file: " + file.getAbsolutePath() ), "Could not delete file!" );
                    }
                }
                else
                {
                    return;
                }


            }

            FileOutputStream outputStream = new FileOutputStream( dict );
            dictionaryFile = new DictionaryFile( outputStream );

            dictionaryFile.parseContent(
                    new Pair<List<Language>, List<OriginalText>>(
                            new ArrayList<Language>( Translator.getDictionary().getLanguages() ),
                            new ArrayList<OriginalText>( Translator.getDictionary().getEntries() )
                    )
            );

            Toast.toast( "Dictionary saved!" );
        }
        catch ( Exception e )
        {
            new ExceptionDialog( e, "Could not save file! :c" );
        }


    }

    public void addDictionaryFile( ActionEvent event )
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle( "Open dictionary.xml" );
        fileChooser.setInitialDirectory( new File( Settings.getInstance().getPath( Settings.ESetting.LAST_OPEN ) ) );
        fileChooser.setSelectedExtensionFilter( new FileChooser.ExtensionFilter( "XML (*.xml)", "*.xml" ) );
        File file = fileChooser.showOpenDialog( StageFactory.INSTANCE.createStage() );
        if ( file == null ) return;
        if ( file.isFile() )
        {
            Settings.getInstance().savePath( Settings.ESetting.LAST_OPEN, file.getAbsolutePath() );
            try
            {
                Translator.getDictionary().registerDictionaryFile( new DictionaryFile( new FileInputStream( file ) ) );
            }
            catch ( FileNotFoundException e )
            {
                new ExceptionDialog( e, "Could not open file! :o" );
                return;
            }
            Toast.toast( "Dictionary loaded", 2000 );

            reloadView();
        }
    }

    private void reloadView()
    {
        this.originalTextObservableList.clear();
        this.translationObservableList.clear();

        this.originalTextObservableList.addAll( Translator.getDictionary().getEntries() );
    }

    private List<TranslationText> generate( OriginalText originalText )
    {
        List<TranslationText> result = new ArrayList<>();

        for ( Language language : Translator.getDictionary().getLanguages() )
        {
            Translation translation = originalText.getTranslation( language.getLanguageKey() );
            if ( translation != null )
                result.add( new TranslationText( translation.getLanguage(), translation.getTranslated(), originalText.getText() ) );
            else
                result.add( new TranslationText( language.getLanguageKey(), "", originalText.getText() ) );
        }

        return result;
    }

    private void transform( OriginalText originalText, List<TranslationText> translationTexts, boolean isProtected )
    {
        originalText.getTranslations().clear();
        for ( TranslationText text : translationTexts )
        {
            if ( !text.getTranslatedText().isEmpty() )
                originalText.addTranslation( text.getLanguageKey(), text.getTranslatedText(), !isProtected );
        }
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/