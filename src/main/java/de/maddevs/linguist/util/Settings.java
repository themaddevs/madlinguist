package de.maddevs.linguist.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.File;
import java.util.prefs.Preferences;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 16.06.2017
 */
public final class Settings
{

    @Getter
    private final static Settings instance = new Settings();
    @Getter
    private Preferences preferences;


    public Settings()
    {
        preferences = Preferences.userNodeForPackage( de.maddevs.linguist.Bootstrap.class );
    }

    public String getDefaultPath()
    {
        return System.getProperty( "user.home" );
    }

    public String getPath( ESetting setting )
    {
        return this.preferences.get( setting.getKey(), getDefaultPath() );
    }

    public void savePath( ESetting setting, String filePath )
    {
        File file = new File( filePath );
        if ( !file.isDirectory() )
            filePath = file.getParent();
        preferences.put( setting.getKey(), filePath );
    }

    @AllArgsConstructor
    public enum ESetting
    {
        LAST_SAVE( "last_save_path" ),
        LAST_OPEN( "last_open_path" );

        @Getter
        private String key;
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/