package de.maddevs.linguist.util;


import de.maddevs.linguist.dialogs.ExceptionDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>A Java-Sourcefile scannable for translations</h1>
 * <p>
 * The sourcefile mirrors a file within a
 * java project. This class can extract all
 * original text within a file, that shall be
 * translated.
 *
 * @author Drayke
 * @version 1.0
 * @since 15.06.2017
 */
public final class SourceFile extends File
{

    public SourceFile( File path )
    {
        super( path.getAbsolutePath() );
    }

    public SourceFile( String pathname )
    {
        super( pathname );
    }

    /*
     * Scans for strings that needs to be translated. A regular expression
     * is used for that covering all common cases for: tr(Text);
     *
     * @note If some translations can't be found, let me know to improve the regex.
     * @return A Set with unique original text entries
     */
    public HashSet<String> scanForTranslations()
    {
        final String trRegex = "t{1}r{1}\\({1}(.+?);{1}"; //Used to discover the content of tr(...); calls
        //final String regex = "\\bt{1}r{1}\\s*\\(.*\"(.+?)\"\\s*.*\\)";

        HashSet<String> originals = new HashSet<>();    //for preventing duplicates
        String fileContent;
        try
        {
            Scanner scanner = new Scanner( this, "UTF-8" );
            fileContent = scanner.useDelimiter( "\\Z" ).next(); //Read whole file into a string

            List<String> matched = new ArrayList<>();

            //Get all contents of tr(...) methods
            Matcher matcher = Pattern.compile( trRegex ).matcher( fileContent );
            while ( matcher.find() )
            {
                matched.add( matcher.group( 1 ) );
            }

            //Evaluate content
            for ( String trContent : matched )
            {
                //Trim string for startsWith() methods
                trContent = trContent.trim();
                String extractedContent = null;

                final int length = trContent.length();

                int count = length - trContent.replace( "\"", "" ).length();


                //There is no translation to extract
                if ( count < 2 ) continue;

                //First case:
                //If method content starts with ". Which could lead to
                //tr("text"); or
                //tr("en","text",....);
                if ( trContent.startsWith( "\"" ) )
                {
                    //Case: tr("text");
                    if ( !hasArgs( trContent ) )
                    {
                        extractedContent = extractContent( trContent );
                    }
                    //Case: tr("en","text");
                    else
                    {
                        //Take the second parameter wrapped in "..";
                        extractedContent = extractContent( trContent, 1 );
                    }
                }
                //2nd Case:
                //If the first argument contains a method call, which means a opening bracket
                //comes before a argument separating comma
                //tr(language(player),"text",...);
                else if ( trContent.indexOf( "(" ) >= 0 && trContent.indexOf( "(" ) < trContent.indexOf( "," ) )
                {
                    //Count Start: ( and the End: )
                    int start = 0;
                    int end = 0;

                    //Iterate whole string and find the "," which is separating
                    //the translation argument from the language
                    for ( int i = trContent.indexOf( '(' ); i < length; i++ )
                    {
                        switch ( trContent.charAt( i ) )
                        {
                            case '(':
                                start++;
                                break;
                            case ')':
                                end++;
                                break;
                        }
                        //If there are as much ( as ) characters, the end of the
                        //method call has been reached
                        if ( start == end )
                        {
                            //Found content!
                            extractedContent = extractContent( trContent.substring( i, length ) );
                            break;
                        }
                    }
                }
                //3rd Case:
                //First argument is a variable
                //tr(language,"text",...);
                else if ( hasArgs( trContent ) )
                {
                    extractedContent = extractContent( trContent );
                }

                if(extractedContent != null)
                    originals.add( extractedContent );

            }

        }
        catch ( Exception e )
        {
            new ExceptionDialog( e );
        }

        return originals;
    }

    /**
     * Returns true if the content contains a ," combination.
     * This combination indicated, that the second argument is the
     * translation string. tr(something,"translation",arg1,arg2);
     *
     * @param trContent
     *
     * @return
     */
    private boolean hasArgs( String trContent )
    {
        final String argsRegex = "(,)\\s*\"{1}"; // searches for ,"  -> tr(lang[,"]text",arg,arg);
        Matcher matcher = Pattern.compile( argsRegex ).matcher( trContent );
        return matcher.find();
    }


    private String extractContent( String string )
    {
        return extractContent( string, 1 );
    }

    private String extractContent( String string, int occurence )
    {
        final String contentRegex = "\\\"(.*?)\\\""; //Used to get content inside string deklaration --> "..."
        Matcher contentMatcher = Pattern.compile( contentRegex ).matcher( string );
        if ( contentMatcher.find() )
            return contentMatcher.group( occurence );

        return null;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/