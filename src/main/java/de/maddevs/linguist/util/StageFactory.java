package de.maddevs.linguist.util;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 14.06.2017
 */
public enum StageFactory
{
    INSTANCE;

    private final ObservableList<Stage> openStages = FXCollections.observableArrayList();
    private final ObjectProperty<Stage> currentStage = new SimpleObjectProperty<>( null );

    public ObservableList<Stage> getOpenStages()
    {
        return openStages;
    }

    public final ObjectProperty<Stage> currentStageProperty()
    {
        return this.currentStage;
    }

    public final Stage getCurrentStage()
    {
        return this.currentStageProperty().get();
    }

    public final void setCurrentStage( final Stage currentStage )
    {
        this.currentStageProperty().set( currentStage );
    }

    public void registerStage( Stage stage )
    {
        stage.addEventHandler( WindowEvent.WINDOW_SHOWN, e -> openStages.add( stage ) );
        stage.addEventHandler( WindowEvent.WINDOW_HIDDEN, e -> openStages.remove( stage ) );
        stage.focusedProperty().addListener( ( obs, wasFocused, isNowFocused ) ->
        {
            if ( isNowFocused )
            {
                currentStage.set( stage );
            }
            else
            {
                currentStage.set( null );
            }
        } );
    }

    public Stage createStage()
    {
        Stage stage = new Stage();
        registerStage( stage );
        return stage;
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/