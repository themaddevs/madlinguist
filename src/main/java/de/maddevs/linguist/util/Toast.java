package de.maddevs.linguist.util;

import de.maddevs.linguist.dialogs.ExceptionDialog;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 16.06.2017
 */
public final class Toast
{

    private final static Stage ownerStage = StageFactory.INSTANCE.createStage();

    public static void toast( String toastMsg, int toastDelay, int fadeInDelay, int fadeOutDelay )
    {
        Stage toastStage = new Stage();
        toastStage.initOwner( ownerStage );
        toastStage.setResizable( false );
        toastStage.initStyle( StageStyle.TRANSPARENT );

        Text text = new Text( toastMsg );
        text.setFont( Font.font( "Verdana", 36 ) );
        text.setFill( Color.DIMGRAY );
        text.setWrappingWidth( 380 );

        //size toast to message
        double height = text.getLayoutBounds().getHeight() + 100;
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        //set Stage boundaries to the lower right corner of the visible bounds of the main screen
        toastStage.setX( primaryScreenBounds.getMinX() + primaryScreenBounds.getWidth() - 400 );
        toastStage.setY( primaryScreenBounds.getMinY() + primaryScreenBounds.getHeight() - height );
        toastStage.setWidth( 400 );
        toastStage.setHeight( height );


        StackPane root = new StackPane( text );
        root.setStyle( "-fx-background-radius: 10; -fx-background-color: rgba(0, 0, 0, 0.2); -fx-padding: 50px;" );
        root.setOpacity( 0 );

        Scene scene = new Scene( root );
        scene.setFill( Color.TRANSPARENT );
        toastStage.setScene( scene );
        toastStage.show();

        Timeline fadeInTimeline = new Timeline();
        KeyFrame fadeInKey1 = new KeyFrame( Duration.millis( fadeInDelay ), new KeyValue( toastStage.getScene().getRoot().opacityProperty(), 1 ) );
        fadeInTimeline.getKeyFrames().add( fadeInKey1 );
        fadeInTimeline.setOnFinished( ( ae ) ->
        {
            new Thread( () ->
            {
                try
                {
                    Thread.sleep( toastDelay );
                }
                catch ( InterruptedException e )
                {
                    new ExceptionDialog( e );
                }
                Timeline fadeOutTimeline = new Timeline();
                KeyFrame fadeOutKey1 = new KeyFrame( Duration.millis( fadeOutDelay ), new KeyValue( toastStage.getScene().getRoot().opacityProperty(), 0 ) );
                fadeOutTimeline.getKeyFrames().add( fadeOutKey1 );
                fadeOutTimeline.setOnFinished( ( aeb ) -> toastStage.close() );
                fadeOutTimeline.play();
            } ).start();
        } );
        fadeInTimeline.play();
    }


    public static void toast( String message, int duration )
    {
        toast( message, duration, 500, 500 );
    }

    public static void toast( String message )
    {
        toast( message, 3500 );
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/