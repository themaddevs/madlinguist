package de.maddevs.linguist.util;

import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.util.List;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 15.06.2017
 */
public final class ListDialog<T> extends Dialog
{

    public ListDialog( String title, String description, List<T> list )
    {
        Alert alert = new Alert( Alert.AlertType.INFORMATION );
        alert.setTitle( title );
        alert.setContentText( description );

        // Create expandable Exception.
        StringBuilder builder = new StringBuilder();
        for ( T i : list )
        {
            builder.append( i.toString() );
            builder.append( "\n" );
        }
        String string = builder.toString();


        TextArea textArea = new TextArea( string.isEmpty() ? "NONE." : string );
        textArea.setEditable( false );
        textArea.setWrapText( true );

        textArea.setMaxWidth( Double.MAX_VALUE );
        textArea.setMaxHeight( Double.MAX_VALUE );
        GridPane.setVgrow( textArea, Priority.ALWAYS );
        GridPane.setHgrow( textArea, Priority.ALWAYS );

        GridPane expContent = new GridPane();
        expContent.setMaxWidth( Double.MAX_VALUE );
        expContent.add( textArea, 0, 1 );

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent( expContent );
        alert.getDialogPane().setExpanded( true );

        alert.showAndWait();
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/