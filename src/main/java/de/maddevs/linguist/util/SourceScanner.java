package de.maddevs.linguist.util;

import de.maddevs.translator.core.OriginalText;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 15.06.2017
 */
public final class SourceScanner
{

    public static final int SCAN_DEPTH = 10;

    @Getter
    private final static SourceScanner instance = new SourceScanner();

    private SourceScanner()
    {
    }

    public List<OriginalText> scan( File path )
    {

        HashSet<String> strings = new HashSet<>();
        if ( path.isDirectory() )
        {
            try
            {
                List<Path> files = new ArrayList<>();
                Files.find( Paths.get( path.toURI() ), SCAN_DEPTH, ( filePath, basicFileAttributes ) -> basicFileAttributes.isRegularFile() )
                        .collect( Collectors.toCollection( () -> files ) );

                for ( Path p : files )
                {
                    SourceFile sourceFile = new SourceFile( p.toFile() );
                    strings.addAll( sourceFile.scanForTranslations() );
                }
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }
        }
        else
        {
            SourceFile sourceFile = new SourceFile( path );
            strings.addAll( sourceFile.scanForTranslations() );
        }

        ArrayList<OriginalText> originalTexts = new ArrayList<>();
        for ( String s : strings )
        {
            originalTexts.add( new OriginalText( s ) );
        }

        return originalTexts;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/