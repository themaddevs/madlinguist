package de.maddevs.linguist.dialogs;


import de.maddevs.translator.core.Language;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.io.IOException;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 16.06.2017
 */
public final class CreateLanguageDialog extends Dialog<Language>
{


    @FXML // fx:id="inputName"
    private TextField inputName; // Value injected by FXMLLoader

    @FXML // fx:id="inputName"
    private TextField inputShortcode; // Value injected by FXMLLoader

    @FXML // fx:id="actionParent"
    private HBox actionParent; // Value injected by FXMLLoader

    @FXML // fx:id="cancelButton"
    private Button cancelButton; // Value injected by FXMLLoader

    @FXML // fx:id="okParent"
    private HBox okParent; // Value injected by FXMLLoader

    @FXML // fx:id="okButton"
    private Button okButton; // Value injected by FXMLLoader

    @FXML // fx:id="icon"
    private ImageView icon; // Value injected by FXMLLoader
    private FXMLLoader mLLoader;

    public CreateLanguageDialog()
    {

        setTitle( "Register a language" );

        try
        {
            FXMLLoader loader = new FXMLLoader( getClass().getClassLoader().getResource( "ui/LanguageDialog.fxml" ) );
            loader.setController( this );
            DialogPane newPane = loader.load();
            setDialogPane( newPane );
        }
        catch ( IOException e )
        {
            new ExceptionDialog( e );
        }

        setResultConverter( param ->
        {

            if ( param.getButtonData() == ButtonBar.ButtonData.CANCEL_CLOSE )
            {
                return null;
            }

            String name = inputName.getText();
            String shortcode = inputShortcode.getText();

            if ( name.isEmpty() | shortcode.isEmpty() ) return null;

            return new Language( shortcode, name );
        } );

    }

    @FXML
    void acceptDialog( ActionEvent event )
    {
        String name = inputName.getText();
        String shortcode = inputShortcode.getText();

        if ( name.isEmpty() | shortcode.isEmpty() ) return;

        setResult( new Language( shortcode, name ) );
    }

    @FXML
    void cancelDialog( ActionEvent event )
    {
        setResult( null );
        close();
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/