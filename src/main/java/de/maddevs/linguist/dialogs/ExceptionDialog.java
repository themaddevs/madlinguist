package de.maddevs.linguist.dialogs;

import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * <h1>TranslatorAPI</h1>
 * <p>
 * __DESCRIPTION__
 *
 * @author Drayke
 * @version 1.0
 * @since 14.06.2017
 */
public final class ExceptionDialog extends Dialog
{

    public ExceptionDialog( Exception ex )
    {
        this( ex, "Keine Fehler details vorhanden." );
    }

    public ExceptionDialog( Exception ex, String string )
    {
        Alert alert = new Alert( Alert.AlertType.ERROR );
        alert.setTitle( "Exception Dialog" );
        alert.setHeaderText( "Oh nein, ein Fehler ist aufgetreten!" );
        alert.setContentText( string );

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter( sw );
        ex.printStackTrace( pw );
        String exceptionText = sw.toString();

        Label label = new Label( "Der exception stacktrace:" );

        TextArea textArea = new TextArea( exceptionText );
        textArea.setEditable( false );
        textArea.setWrapText( true );

        textArea.setMaxWidth( Double.MAX_VALUE );
        textArea.setMaxHeight( Double.MAX_VALUE );
        GridPane.setVgrow( textArea, Priority.ALWAYS );
        GridPane.setHgrow( textArea, Priority.ALWAYS );

        GridPane expContent = new GridPane();
        expContent.setMaxWidth( Double.MAX_VALUE );
        expContent.add( label, 0, 0 );
        expContent.add( textArea, 0, 1 );

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent( expContent );

        alert.show();
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/